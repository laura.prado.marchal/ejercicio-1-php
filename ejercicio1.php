<?php
    echo 'Así sería con un bucle: ';
    $dia = 6;
    if ($dia == 1) {
        echo 'Es lunes';
    } elseif ($dia == 2) {
        echo 'Es martes';
    } elseif ($dia == 3) {
        echo 'Es miércoles'; 
    } elseif ($dia == 4) {
        echo 'Es jueves'; 
    } elseif ($dia == 5) {
        echo 'Es viernes'; 
    } elseif ($dia == 6) {
        echo 'Es sábado'; 
    } elseif ($dia == 7) {
        echo 'Es domingo'; 
    }
?>
</br>
<?php
    echo 'Así sería con una función: ';
    function dia($convertir) {
        if ($convertir == 1) {
            echo 'Es lunes';
        } elseif ($convertir == 2) {
            echo 'Es martes';
        } elseif ($convertir == 3) {
            echo 'Es miércoles'; 
        } elseif ($convertir == 4) {
            echo 'Es jueves'; 
        } elseif ($convertir == 5) {
            echo 'Es viernes'; 
        } elseif ($convertir == 6) {
            echo 'Es sábado'; 
        } elseif ($convertir == 7) {
            echo 'Es domingo';
        }
    }
    dia(3);
?>